import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Factory} from '../../../models/factory';
import {FactoryService} from '../../services/factory.service';

@Component({
  selector: 'app-factorys',
  templateUrl: './factorys.component.html',
  styleUrls: ['./factorys.component.scss']
})
export class FactorysComponent implements OnInit {

  @ViewChild('name1') name1: ElementRef
  @ViewChild('buttonElement') search: ElementRef
  totalFactorys: Factory[]
  factoryContent: string[]
  showInput: boolean
  showBtn: boolean;
  showBtnSearch: boolean
  showBtnCreate: boolean
  factorys: Factory[]
  constructor(private factoryService: FactoryService) {
    this.showBtnSearch = false
    this.showBtnSearch = false
    this.factoryContent = [
    ]
    this.showBtn = false
    this.showInput = false
    this.factoryService.getFactory().subscribe(x => {
      this.factorys = x.data
    })
  }

  ngOnInit() {
  }



}
