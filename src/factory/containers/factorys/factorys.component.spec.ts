import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactorysComponent } from './factorys.component';

describe('FactorysComponent', () => {
  let component: FactorysComponent;
  let fixture: ComponentFixture<FactorysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactorysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactorysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
