import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

const FACTORY_URL = `${environment.apiUrl}/factory`
@Injectable({
  providedIn: 'root'
})
export class FactoryService {

  constructor(private http: HttpClient) { }

  getFactory(): Observable<any>{
    return this.http.get<any>(FACTORY_URL)
  }
}
