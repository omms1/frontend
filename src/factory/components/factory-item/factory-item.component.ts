import {Component, Input, OnInit} from '@angular/core';
import {Factory} from '../../../models/factory';

@Component({
  selector: 'app-factory-item',
  templateUrl: './factory-item.component.html',
  styleUrls: ['./factory-item.component.scss']
})
export class FactoryItemComponent implements OnInit {

  @Input() factory: Factory
  @Input() num: number
  constructor() { }

  ngOnInit() {
  }

}
