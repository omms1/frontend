import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FactorysComponent } from './containers/factorys/factorys.component';
import {RouterModule, Routes} from '@angular/router';
import { FactoryComponent } from './containers/factory/factory.component';
import { FactoryItemComponent } from './components/factory-item/factory-item.component';
import { FactoryFormComponent } from './components/factory-form/factory-form.component';
import {HttpClientModule} from '@angular/common/http';

export const factoryRoutes: Routes = [
  {path: '', component: FactorysComponent},
  {path: ':id', component: FactoryComponent},
  {path: 'new', component: FactoryComponent}
]


@NgModule({
  declarations: [FactorysComponent, FactoryComponent, FactoryItemComponent, FactoryFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(factoryRoutes),
    HttpClientModule
  ]
})
export class FactoryModule { }
