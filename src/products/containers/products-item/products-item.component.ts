import { Component, OnInit } from '@angular/core';
import {Order} from '../../model/order';
import {OrderService} from '../../services/order.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../auth/shared/services/auth.service';

@Component({
  selector: 'app-products-item',
  templateUrl: './products-item.component.html',
  styleUrls: ['./products-item.component.scss']
})
export class ProductsItemComponent implements OnInit {

  order: Order
  constructor(private orderService: OrderService,
              private routes: ActivatedRoute,
              private authService: AuthService,
              private router: Router) {
    this.orderService.getOrder().subscribe(msg => {
      const id = this.routes.snapshot.params.id
      let order = null
      if (id === 'new') {
        order = {}
      } else {
        order = msg.data.find(x => x.id === id)
      }
      this.order = order
    })
  }

  ngOnInit() {
  }

  createOrder(order: Order) {
    this.orderService.createOrder(order).subscribe(x => {
      this.router.navigate(['/products', order.id])
    })
  }

  updateOrder(order: Order) {
    this.orderService.updateOrder(order).subscribe(() => {
      this.router.navigate(['/'])
    })
  }

  deleteOrder(order: Order) {
    const alert = window.confirm(`Are you sure you wanna delete this order?`)
    if (alert && order && order.id) {
      this.orderService.deleteOrder(order).subscribe(() => {
        this.router.navigate(['/products'])
      })
    }
    if (alert && !order.id) {
      this.router.navigate(['/products'])
    }
  }
}
