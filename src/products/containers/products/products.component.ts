import {Component, ElementRef, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {OrderService} from '../../services/order.service';
import {Order} from '../../model/order';
import {AuthService} from '../../../auth/shared/services/auth.service';
import {Role} from '../../../models/role';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  @ViewChild('name1') name1: ElementRef
  @ViewChild('buttonElement') search: ElementRef
  orders: Order[]
  order: Order
  totalOrder: Order[]
  orderContent: string[]
  showInput: boolean
  showBtn: boolean;
  showBtnSearch: boolean
  showBtnCreate: boolean
  constructor(private orderService: OrderService,
              private authService: AuthService) {
    this.showBtnSearch = false
    this.showBtnSearch = false
    this.orderContent = [
      'orderName', 'orderPosition', 'invoiceNumber', 'customerName', 'poNumber', 'factoryName'
    ]
    this.showBtn = false
    this.showInput = false
    this.orderService.getOrder().subscribe(x => {
      console.log('curry1')
      this.orders = x.data
      this.totalOrder = this.orders
    })

  }

  get inputControl() {
    return this.name1.nativeElement
  }

  get searchControl() {
    return this.search.nativeElement
  }
  ngOnInit() {
    setTimeout(() => {
      if (this.authService.currentUserValue &&
        this.authService.currentUserValue.user &&
        this.authService.currentUserValue.user.role.some(r => r.toLowerCase() === Role.User.toLowerCase())){
        console.log('curry')
        this.orders = this.totalOrder
        this.orders = this.orders.filter(x =>
          x.customerId === this.authService.currentUserValue.user.companyId)
      }
    }, 70)

  }


  onSearch(str1: string, str2: string) {
    if (!!str2) {
      this.orders = this.totalOrder
      this.orders = this.orders.filter(x => x[str1] === str2)
      this.showBtn = true
    }
  }

  onTotalOrder() {
    this.orders = this.totalOrder
    this.showBtn = false
  }

  showSearch() {
    this.showInput = !this.showInput
  }

  selectName(name: string) {
    this.inputControl.value = name
  }

  judgeName(name: string) {
    if (name === this.inputControl.value) {
      return true
    } else {
      return false
    }
  }

  public get cCompanyCanSee() {
    return  setTimeout(() => {
      if (this.authService.currentUserValue &&
        this.authService.currentUserValue.user &&
        this.orders.some(x => x.customerId === this.authService.currentUserValue.user.companyId)) {
        return true
      } else {return false}
    }, 50)
  }

}
