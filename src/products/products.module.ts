import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './containers/products/products.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { OrderItemComponent } from './components/order-item/order-item.component';
import {ReactiveFormsModule} from '@angular/forms';
import { ProductsItemComponent } from './containers/products-item/products-item.component';
import { OrderFormComponent } from './components/order-form/order-form.component';

export const routes: Routes = [
  {path: '', component: ProductsComponent},
  {path: 'new', component: ProductsItemComponent},
  {path: ':id', component: ProductsItemComponent}
]
@NgModule({
  declarations: [ProductsComponent, OrderItemComponent, ProductsItemComponent, OrderFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class ProductsModule { }
