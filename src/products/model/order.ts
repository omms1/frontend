
export interface DownPayment { // 工厂预付款
  factoryId: string;
  amount: number;
  notes: string;
}

export interface BlancePayment { // 工厂尾款
  factoryId: string;
  amount: number;
  notes: string;
}

export enum OrderStatus {
  signWithC, // B跟C签订合同
  manufacturing, // 工厂生产中
  delivery, // 发货
  returnTax, // 退税
  done// 留存
}
export interface Order {
  id: string;
  // ************
  // 公共字段
  // ************
  note: string;

  status: OrderStatus

  // ************
  // C与B签订合同阶段
  // ************
  invoiceNumber: string;

  customerId: string;

  factoryId: string[]

  poNumber: string;

  balanceUsd: number;

  balanceCad: number;

  flightInformation: string;

  cabinetType: string;

  toASubOrderIds: string[];

  expectedDeliveryDate: number


  factoryAdvancedPayment: DownPayment[];

  factoryFinalPayment: BlancePayment[]
}
