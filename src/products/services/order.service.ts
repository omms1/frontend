import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Order} from '../model/order';
import {environment} from '../../environments/environment';
import {User} from '../../models/user';
import {Role} from '../../models/role';

const ORDER_URL = `${environment.apiUrl}/order`
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  orders: Order[]
  constructor(private http: HttpClient) {
  }

  getOrder(): Observable<any>{
    return this.http.get<any>(ORDER_URL)
  }

  createOrder(order: Order): Observable<Order>{
    return this.http.post<Order>(ORDER_URL, order)
  }

  updateOrder(order: Order): Observable<Order>{
    return this.http.put<Order>(`${ORDER_URL}/${order.id}`, order)
  }

  deleteOrder(order: Order): Observable<Order>{
    return this.http.delete<Order>(`${ORDER_URL}/${order.id}`)
  }
}
