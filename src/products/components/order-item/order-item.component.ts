import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Order} from '../../model/order';
import {Router} from '@angular/router';
import {AuthService} from '../../../auth/shared/services/auth.service';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss']
})
export class OrderItemComponent implements OnInit, OnChanges {

  @Input() order: Order
  @Input() num: Number
  showHight: boolean
  constructor(private router: Router,
              private authService: AuthService) {
    this.showHight = false
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  viewOrder() {
    this.router.navigate(['/products', this.order.id])
  }

  showHighLight1() {
    this.showHight = true
  }

  showHighLight2() {
    this.showHight = false
  }
}
