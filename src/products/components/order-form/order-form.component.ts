import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {BlancePayment, DownPayment, Order} from '../../model/order';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit, OnChanges {

  @Input() order: Order
  @Input() canEdit: boolean
  @Output() createEvent: EventEmitter<Order> = new EventEmitter()
  @Output() updateEvent: EventEmitter<Order> = new EventEmitter()
  @Output() deleteEvent: EventEmitter<Order> = new EventEmitter()
  myForm: FormGroup
  showBtn: boolean
  constructor(private fb: FormBuilder,
              private router: Router) {
    this.showBtn = false
    this.myForm = this.fb.group({
      id: [''],
      note: [''],
      status: [''],
      invoiceNumber: [''],
      customerId: [''],
      factoryId: [''],
      poNumber: [''],
      balanceUsd: [''],
      balanceCad: [''],
      flightInformation: [''],
      cabinetType: [''],
      toASubOrderIds: [[]],
      expectedDeliveryDate: [''],
      factoryAdvancedPayment: [[]],
      factoryFinalPayment: [[]]
    })
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.order && this.order.id) {
      this.showBtn = true
      this.myForm.patchValue(this.order)
    }
  }

  createOrder() {
    const {value, valid} = this.myForm
    if (valid) {
      this.createEvent.emit(value)
    }
  }

  updateOrder() {
    const {value, valid, touched} = this.myForm
    if (valid && touched) {
      this.updateEvent.emit(value)
    }
  }

  deleteOrder() {
    const {value, valid} = this.myForm
    this.deleteEvent.emit(value)
  }

}
