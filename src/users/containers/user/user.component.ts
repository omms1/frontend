import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../services';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../models/user.model';
import {Validators} from '@angular/forms';
import {elementStart} from '@angular/core/src/render3';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  eachUser: User = undefined;
  showInfo: boolean = false
  errmsg: boolean = false

  constructor(private UserService: UsersService,
              private route: ActivatedRoute,
              private rout: Router) {
    const id = this.route.snapshot.params.id;
    if ('new' === id) {
      console.log('gointo usercomponenet')
      this.eachUser = {
        id: null,
        userName: null,
        useremail: null,
        password: null,
        phone1: null,
        phone2: null,
        role: [],
        companyId: null,
        duty: null,
        createdAt: null,
        updateAt: null,
        deleteAt: null,
        deleted: null,
        active: null
      }
    } else {
      this.UserService.getUser().subscribe(x => {
        const user: User = x.data.find( r => r.id === id)
        this.eachUser = user
        console.log('user:', this.eachUser)
      })
    }
  }

  ngOnInit() {
  }

  onEdit(newUser: User) {
    this.showInfo = true
    this.UserService.updateUser(newUser).subscribe(show => {this.errmsg = false
      this.rout.navigate(['/user'])}, error => this.errmsg = true)
  }

  onCreate(newUser: User) {
    this.showInfo = true
    console.log(newUser)
    this.UserService.createUser(newUser).subscribe( x => {this.errmsg = false
    this.rout.navigate(['/user'])})
  }

  onDelete(user: User){
    const alert = window.confirm(`Confirm delete ${user.userName.toLocaleUpperCase()}?`)
    if (alert && user && user.id) {
      this.UserService.deleteUser(user).subscribe( x => this.rout.navigate(['/user']))
    } else {
      this.rout.navigate(['/user'])
    }
  }

}
