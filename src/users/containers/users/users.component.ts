import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services';
import {User} from '../../models/user.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  allUser: User[]
  constructor(private userService: UsersService,
              private router: Router) { }

  ngOnInit() {
    this.userService.getUser().subscribe(user => {this.allUser = user.data})
  }

  onCreateUser() {
    this.router.navigate(['/user', 'new'])
  }
}
