export interface User {
  id?: string;
  userName?: string;
  password?: string;
  phone1?: number;
  phone2?: number;
  useremail?: number;
  role?: string[];
  companyId?: string;
  duty?: string;
  createdAt?: Date;
  updateAt?: number;
  deleteAt?: number;
  deleted?: boolean;
  active?: boolean;
}

export interface MessageError {
  data: any
  code: number
  msg: string
}
