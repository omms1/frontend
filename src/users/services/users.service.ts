import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MessageError, User} from '../models/user.model';
import {Observable} from 'rxjs';

const serverUrl = `http://localhost:3838/user`

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }
  getUser(): Observable<MessageError> {
      return this.http.get<MessageError>(serverUrl)
  }
  createUser(payload: User): Observable<User> {
    return this.http.post<User>(serverUrl, payload)
  }
  updateUser(payload: User): Observable<User> {
    return this.http.patch<User>(`${serverUrl}/${payload.id}`, payload)
  }
  deleteUser(payload: User): Observable<MessageError>{
    return this.http.delete<any>(`${serverUrl}/${payload.id}`)
  }
}
