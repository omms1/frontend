import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './containers/users/users.component';
import { UserComponent } from './containers/user/user.component';
import { UserFormComponent } from './component/user-form/user-form.component';
import { UserItemComponent } from './component/user-item/user-item.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import * as fromusercontainers from './containers';
import {ReactiveFormsModule} from '@angular/forms';
import { RoleTypeComponent } from './component/role-type/role-type.component';

export const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: fromusercontainers.UsersComponent
  },
  {
    path: ':id',
    component: fromusercontainers.UserComponent
  },
  // {
  //   path: 'new',
  //   component: fromusercontainers.UserComponent
  // }
]

@NgModule({
  declarations: [UsersComponent, UserComponent, UserFormComponent, UserItemComponent, RoleTypeComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule
  ]
})
export class UsersModule { }
