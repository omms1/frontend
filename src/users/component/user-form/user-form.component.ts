import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {User} from '../../models/user.model';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit, OnChanges{
  @Input() userDetail: User;
  @Output() editEvent: EventEmitter<User>
  @Output() createEvent: EventEmitter<User>
  @Output() deleteEvent: EventEmitter<User>
  form: FormGroup;
  updateOrCreate: boolean
  constructor(private rout: Router,
              private fb: FormBuilder) {
    this.editEvent = new EventEmitter()
    this.createEvent = new EventEmitter()
    this.deleteEvent = new EventEmitter()

    console.log('constrcu', this.userDetail)
    this.form = this.fb.group({
      id: [''],
      userName: [''],
      useremail: [''],
      password: [''],
      phone1: [''],
      phone2: [''],
      email: [''],
      role: this.fb.array([]),
      companyId : [''],
      duty: [''],
      createdAt: [''],
      updateAt: [''],
      deleteAt: [''],
      deleted: [''],
      active: ['']
    })
  }

  ngOnInit() {
    // if (this.userDetail && this.userDetail.id){
    //   console.log('userform oninit->')
    //   this.form.patchValue(this.userDetail)
    // }
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('onchange:', this.userDetail)
    if (this.userDetail && this.userDetail.id) {
      this.form.patchValue(this.userDetail)
      this.updateOrCreate = true
    } else {
      this.updateOrCreate = false
    }
  }
  submitUser() {
    const {value, valid, touched} = this.form
    if (touched && valid) {
      console.log(this.form)
      this.updateOrCreate ? this.editEvent.emit(value) : this.createEvent.emit(value)
    }
  }

  deleteUser() {
    const {value, valid} = this.form
    this.deleteEvent.emit(value)
  }









}
