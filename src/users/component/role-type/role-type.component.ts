import {Component, forwardRef, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

export  const ROLE_TYPE_CONTROL_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RoleTypeComponent),
  multi: true
}

@Component({
  selector: 'app-role-type',
  providers: [ROLE_TYPE_CONTROL_ACCESSOR],
  templateUrl: './role-type.component.html',
  styleUrls: ['./role-type.component.scss']
})
export class RoleTypeComponent implements ControlValueAccessor {
  roles: string[] = ['admin', 'superadmin', 'auser', 'buser', 'cuser']
  value: string = ''
  private onTounch: Function
  private onModelChange: Function
  isDisable: boolean = false
  constructor() { }

  registerOnChange(fn: any): void {
    this.onModelChange = fn
  }

  registerOnTouched(fn: any): void {
    this.onTounch = fn
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisable = isDisabled
  }

  writeValue(role: string): void {
    this.value = role
  }



}
