import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.scss']
})
export class UserItemComponent implements OnInit {
  @Input() userItem: User
  @Input() num: number
  form: FormGroup
  constructor(private fb: FormBuilder,
              private route: Router ) {
    this.form = this.fb.group({
      id: ['', Validators.required],
      userName: [''],
      useremail: [''],
      password: [''],
      phone1: [''],
      phone2: [''],
      email: [''],
      role: this.fb.array([]),
      companyId : [''],
      duty: [''],
      createdAt: [''],
      updateAt: [''],
      deleteAt: [''],
      deleted: [''],
      active: ['']
    })
  }

  ngOnInit() {
    // this.form.patchValue(this.userItem)

  }

  get getRoleForms() {
    return this.form.get('role') as FormArray
  }

  viewUser() {
    this.route.navigate(['/user', this.userItem.id])
  }



}
