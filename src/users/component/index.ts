import {UserFormComponent} from './user-form/user-form.component';
import {UserItemComponent} from './user-item/user-item.component';

export const usercomponent: any[] = [UserFormComponent, UserItemComponent]

export * from './user-form/user-form.component';
export * from './user-item/user-item.component';
