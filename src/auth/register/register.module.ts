import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './containers/register/register.component';
import {RouterModule, Routes} from '@angular/router';

export const registerRoutes: Routes = [
  {path: '', component: RegisterComponent}
]
@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(registerRoutes)
  ]
})
export class RegisterModule { }
