import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

export const routes: Routes = [
  {path: 'auth', children: [
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', loadChildren: './login/login.module#LoginModule'},
      {path: 'logout', loadChildren: './logout/logout.module#LogoutModule'},
      {path: 'register', loadChildren: './register/register.module#RegisterModule'},
    ]}
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ]
})
export class AuthModule { }
