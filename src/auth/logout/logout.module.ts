import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoutComponent } from './containers/logout/logout.component';
import {RouterModule, Routes} from '@angular/router';

export const logoutRoutes: Routes = [
  {path: '', component: LogoutComponent}
]
@NgModule({
  declarations: [LogoutComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(logoutRoutes)
  ]
})
export class LogoutModule { }
