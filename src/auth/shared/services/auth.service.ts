import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {MessageError, User} from '../../../models/user';
import {map} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs';
import {Role} from '../../../models/role';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<User>
  public currentUser: Observable<User>
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(environment.userStoreKey)))
    this.currentUser = this.currentUserSubject.asObservable()
  }

  public get currentUserValue(): any{
    return this.currentUserSubject.value
  }

  public get canEditProduct() {
    if (this.currentUserValue &&
      this.currentUserValue.user &&
      this.currentUserValue.user.role.some(r => r.toLowerCase() === Role.Admin.toLowerCase())
    ) {
      return true
    }
    else {return false}
  }

  public get cCompanyCanSee() {
    if (this.currentUserValue &&
      this.currentUserValue.user &&
      this.currentUserValue.user.role.some(r => r.toLowerCase() === Role.Admin.toLowerCase())
    ) {
      return true
    }
    else {return false}
  }

  onLogin(useremail, password) {
    return  this.http.post<MessageError>(`${environment.apiUrl}/auth/login`, {useremail, password})
      .pipe(map(res => {
        console.log('angela')
        if (res.data && res.data.token) {
          localStorage.setItem(environment.userStoreKey, JSON.stringify(res.data))
          this.currentUserSubject.next(res.data)
        }
        return res
      }))
  }

  logout() {
    localStorage.removeItem(environment.userStoreKey)
    this.currentUserSubject.next(null)
  }
}
