import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss']
})
export class AuthFormComponent implements OnInit {

  @Output() submitEvent = new EventEmitter<FormGroup>()
  myForm: FormGroup
  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({
      useremail: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    })
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.myForm.valid) {
      this.submitEvent.emit(this.myForm)
    }
  }
}
