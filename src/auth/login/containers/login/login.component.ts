import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth.service';
import {Router} from '@angular/router';
import {MessageError} from '../../../../models/user';
import {FactoryType} from '../../../../models/factory';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  }

  loginUser(evt: FormGroup) {
    const {useremail, password} = evt.value
    this.authService.onLogin(useremail, password).subscribe((obj: MessageError) => {
      console.log('loginUser->', obj)
      if (this.authService.currentUserValue.user.companyType === FactoryType.customer) {
        this.router.navigate(['/products'])
      } else if (this.authService.currentUserValue.user.companyType === FactoryType.amanufacturer) {
        this.router.navigate(['/to-asub-order'])
      }
    })
  }
}
