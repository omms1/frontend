export enum FactoryType {
  amanufacturer  = 'manufacturer',
  bcompany = 'myself',
  customer = 'customer'
}

export interface Factory {
  id: string

  type: FactoryType;

  name: string

  note?: string;
}
