import {Part} from './part';
import {Delivery} from './delivery';

export interface ToASubOrder {
  id: string;

  contractNum: string;

  orderId: string;

  contractPriceRmb: number;

  downPaymentRmb: number;

  // @Column() // balance, 可以通过contractPrice - downpaymentPrice
  // balanceRmb: number;

  note: string;

  flightInformation: string;

  // ************
  // 生产阶段
  // ************
  part: Part;

  // ************
  // 发货阶段
  // ************
  delivery: Delivery;
}
