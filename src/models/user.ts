import {Role} from './role';
import {FactoryType} from './factory';

export interface User {
  id: string;

  userName: string;

  password: string;

  phone1: string;

  phone2: string;

  useremail: string;

  role: string[];

  companyType: FactoryType

  companyId: string;

  duty: string;

  createdAt: number;

  updateAt: number;

  deleteAt: number;

  deleted: boolean;

  active: boolean;
}

export interface MessageError {
  data: any
  code: number
  msg: string
}

