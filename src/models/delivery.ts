export interface Delivery {
  // 交付日期, B完成生产的日期
  handOverTime: number;

  // 柜型
  cabinetType: string;

  // 提单客户名
  billOfLadingName: string;

  // 预定发货日期,需要不断追加日期，使用string类型，提前二十天联系生产工厂，系统提供发送信息功能或者邮件
  scheduledDeliveryTime: number;

  // 工厂给发货清单
  factoryDeliveryNotice: boolean;

  // 告工厂入货
  factoryIntoTheGoods: boolean;

  // 发ISF
  sendIsf: boolean;

  // 截单/截港时间
  containerYardCloseTime: number;

  // 上船船期
  boardingDate: number;

  // 是否收到提单
  confirmReceiveBillOfLading: boolean;

  // 发PAULINE清关资料（提单箱单发票）抄送sales3
  sendPauline: boolean;

  // 是否收报关单
  confirmReceiveDeclaration: boolean;

  // NANA是否收到报关单
  nanaReceive: boolean;

  // 开发票信息工厂
  sendInvoiceToFactory: boolean;

  // 工厂开发票数额
  factoryInvoiceBalance: number;

  // 收到发票没有
  receiveInvoice: boolean;

  // 报关单发票匹配
  declarationMatch: boolean;
}
