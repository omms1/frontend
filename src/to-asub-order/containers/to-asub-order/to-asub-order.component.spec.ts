import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToAsubOrderComponent } from './to-asub-order.component';

describe('ToAsubOrderComponent', () => {
  let component: ToAsubOrderComponent;
  let fixture: ComponentFixture<ToAsubOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToAsubOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToAsubOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
