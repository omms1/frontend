import { Component, OnInit } from '@angular/core';
import {ToASubOrder} from '../../../models/to-asub-order';
import {ToAsubOrderService} from '../../services/to-asub-order.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-to-asub-order',
  templateUrl: './to-asub-order.component.html',
  styleUrls: ['./to-asub-order.component.scss']
})
export class ToAsubOrderComponent implements OnInit {

  toASubOrder: ToASubOrder
  constructor(private toASubOrderService: ToAsubOrderService,
              private routes: ActivatedRoute) {
    this.toASubOrderService.getToASubOrder().subscribe(x => {
      const param = this.routes.snapshot.params.id
      let toASubOrder = null
      if (param === 'new') {
        toASubOrder = {}
      } else {
        toASubOrder = x.data.find(x => x.id === param)
      }
      this.toASubOrder = toASubOrder
    })
  }

  ngOnInit() {
  }

}
