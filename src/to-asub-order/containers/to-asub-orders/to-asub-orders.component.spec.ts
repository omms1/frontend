import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToAsubOrdersComponent } from './to-asub-orders.component';

describe('ToAsubOrdersComponent', () => {
  let component: ToAsubOrdersComponent;
  let fixture: ComponentFixture<ToAsubOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToAsubOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToAsubOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
