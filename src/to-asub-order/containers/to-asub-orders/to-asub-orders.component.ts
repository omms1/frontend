import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ToAsubOrderService} from '../../services/to-asub-order.service';
import {ToASubOrder} from '../../../models/to-asub-order';

@Component({
  selector: 'app-to-asub-orders',
  templateUrl: './to-asub-orders.component.html',
  styleUrls: ['./to-asub-orders.component.scss']
})
export class ToAsubOrdersComponent implements OnInit {

  @ViewChild('name1') name1: ElementRef
  @ViewChild('buttonElement') search: ElementRef
  totalToASubOrder: ToASubOrder[]
  toASubOrderContent: string[]
  showInput: boolean
  showBtn: boolean;
  showBtnSearch: boolean
  showBtnCreate: boolean
  toASubOrders: ToASubOrder[]
  constructor(private toASubOrderService: ToAsubOrderService) {
    this.showBtnSearch = false
    this.showBtnSearch = false
    this.toASubOrderContent = [
    ]
    this.showBtn = false
    this.showInput = false
    this.toASubOrderService.getToASubOrder().subscribe(x => {
      this.toASubOrders = x.data
    })
  }

  ngOnInit() {
  }

}
