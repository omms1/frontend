import { TestBed } from '@angular/core/testing';

import { ToAsubOrderService } from './to-asub-order.service';

describe('ToAsubOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToAsubOrderService = TestBed.get(ToAsubOrderService);
    expect(service).toBeTruthy();
  });
});
