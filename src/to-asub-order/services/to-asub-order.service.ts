import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
export const toASubOrder_URL = `${environment.apiUrl}/toASubOrder`
@Injectable({
  providedIn: 'root'
})
export class ToAsubOrderService {

  constructor(private http: HttpClient) { }

  getToASubOrder(): Observable<any>{
    return this.http.get<any>(toASubOrder_URL)
  }
}
