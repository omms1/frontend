import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ToASubOrder} from '../../../models/to-asub-order';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-to-asub-order-form',
  templateUrl: './to-asub-order-form.component.html',
  styleUrls: ['./to-asub-order-form.component.scss']
})
export class ToAsubOrderFormComponent implements OnInit, OnChanges {

  @Input() toASubOrder: ToASubOrder
  myForm: FormGroup
  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({
      id: [''],
      contractNum: [''],
      orderId: [''],
      contractPriceRmb: [''],
      downPaymentRmb: [''],
      note: [''],
      flightInformation: [''],
      part: [''],
      delivery: ['']
    })
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.toASubOrder && this.toASubOrder.id) {
      this.myForm.patchValue(this.toASubOrder)
    }
  }

}
