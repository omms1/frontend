import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToAsubOrderFormComponent } from './to-asub-order-form.component';

describe('ToAsubOrderFormComponent', () => {
  let component: ToAsubOrderFormComponent;
  let fixture: ComponentFixture<ToAsubOrderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToAsubOrderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToAsubOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
