import {Component, Input, OnInit} from '@angular/core';
import {ToASubOrder} from '../../../models/to-asub-order';
import {Router} from '@angular/router';

@Component({
  selector: 'app-to-asub-order-item',
  templateUrl: './to-asub-order-item.component.html',
  styleUrls: ['./to-asub-order-item.component.scss']
})
export class ToAsubOrderItemComponent implements OnInit {

  @Input() num: number
  @Input() toASubOrder: ToASubOrder
  showHight: boolean
  constructor(private router: Router) {
    this.showHight = false
  }

  ngOnInit() {
  }

  showHighLight1() {
    this.showHight = true
  }

  showHighLight2() {
    this.showHight = false
  }

  viewOrder() {
    this.router.navigate(['/to-asub-order', this.toASubOrder.id])
  }

}
