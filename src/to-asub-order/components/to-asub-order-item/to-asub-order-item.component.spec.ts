import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToAsubOrderItemComponent } from './to-asub-order-item.component';

describe('ToAsubOrderItemComponent', () => {
  let component: ToAsubOrderItemComponent;
  let fixture: ComponentFixture<ToAsubOrderItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToAsubOrderItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToAsubOrderItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
