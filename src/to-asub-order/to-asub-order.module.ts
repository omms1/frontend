import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToAsubOrdersComponent } from './containers/to-asub-orders/to-asub-orders.component';
import {RouterModule, Routes} from '@angular/router';
import { ToAsubOrderComponent } from './containers/to-asub-order/to-asub-order.component';
import { ToAsubOrderItemComponent } from './components/to-asub-order-item/to-asub-order-item.component';
import { ToAsubOrderFormComponent } from './components/to-asub-order-form/to-asub-order-form.component';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';

export const toASubOrderRoutes: Routes = [
  {path: '', component: ToAsubOrdersComponent},
  {path: ':id', component: ToAsubOrderComponent},
  {path: 'new', component: ToAsubOrderComponent}
]
@NgModule({
  declarations: [ToAsubOrdersComponent, ToAsubOrderComponent, ToAsubOrderItemComponent, ToAsubOrderFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(toASubOrderRoutes),
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class ToASubOrderModule { }
