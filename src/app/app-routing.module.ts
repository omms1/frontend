import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsersComponent} from '../users/containers';
import {AppComponent} from './app.component';
import {UserItemComponent} from '../users/component';

const routes: Routes = [
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {
    path: 'products', loadChildren: '../products/products.module#ProductsModule'
  },
  {
    path: 'auth', loadChildren: '../auth/auth.module#AuthModule'
  },
  {
    path: 'user', loadChildren: '../users/users.module#UsersModule'
  },
  {
    path: 'factory', loadChildren: '../factory/factory.module#FactoryModule'
  },
  {
    path: 'to-asub-order', loadChildren: '../to-asub-order/to-asub-order.module#ToASubOrderModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
