import { Component } from '@angular/core';
import {AuthService} from '../auth/shared/services/auth.service';
import {Router} from '@angular/router';
interface Navigation {
  link: string
  name: string
  exact: boolean
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  showMenu: boolean
  nav: Navigation[] = [
    {link: '/orders', name: 'New', exact: true},
  ]
  constructor(private auth: AuthService,
              private router: Router) {}
  toggleMenu() {
    if (!this.auth.currentUserValue) {
      this.router.navigate(['/auth/login'])
    } else {
      this.showMenu = ! this.showMenu
    }
  }
}
